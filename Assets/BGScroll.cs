﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGScroll : MonoBehaviour {

    public Vector2 animRate;
    //private Renderer rend;
    private Image image;
    private Vector2 uvOffset;
	// Use this for initialization
	void Start ()
    {
        uvOffset = Vector2.zero;
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (image.enabled)
        {
            uvOffset += (animRate * Time.deltaTime);
            image.material.SetTextureOffset("_MainTex", uvOffset);
        }
	}
}
