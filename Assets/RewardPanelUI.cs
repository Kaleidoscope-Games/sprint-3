﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardPanelUI : MonoBehaviour
{
    private GameManager gm;
    public Sprite ogun, dante, turretta, galactica, karma;

    public Image icon1, icon2;
    private Image icon1Shadow, icon2Shadow;
    public Text respect, respectVal, trust, trustVal, respect2, respectVal2, trust2, trustVal2;


    private void Start()
    {
        gm = GameManager.Instance;
        icon1Shadow = icon1.transform.GetChild(0).GetComponent<Image>();
        icon2Shadow = icon2.transform.GetChild(0).GetComponent<Image>();
    }
    /// <summary>
    /// Clears modular data (not respect and trust text)
    /// then deactivate all objects so the vertical layout group is centred
    /// </summary>
    private void ResetUI()
    {
        icon1.gameObject.SetActive(false);
        icon1.sprite = null;
        icon1Shadow.sprite = null;

        icon2.gameObject.SetActive(false);
        icon2.sprite = null;
        icon2Shadow.sprite = null;


        respect.gameObject.SetActive(false);
        respectVal.text = "";
        respectVal.gameObject.SetActive(false);

        respect2.gameObject.SetActive(false);
        respectVal2.text = "";
        respectVal2.gameObject.SetActive(false);

        trust.gameObject.SetActive(false);
        trustVal.text = "";
        trustVal.gameObject.SetActive(false);

        trust2.gameObject.SetActive(false);
        trustVal2.text = "";
        trustVal2.gameObject.SetActive(false);
    }
    /// <summary>
    /// Converts the int value to a string with a +/- depending on the value.
    /// In addition if the value is zero it will empty the string and disable it's header
    /// </summary>
    /// <param name="value"></param>
    /// <param name="header"></param>
    /// <returns></returns>
    private string ConvertRewardToString(int value, Text header)
    {
        if (value > 0)
        {
            return "+" + value.ToString();
        }
        else if (value < 0)
        {
            //no need to add a "-" symbol because it is included in the string
            return value.ToString();
        }
        else
        {
            header.gameObject.SetActive(false);
            return "";
        }
    }
    private void ActivateRewardA()
    {
        icon1.gameObject.SetActive(true);
        respect.gameObject.SetActive(true);
        respectVal.gameObject.SetActive(true);
        trust.gameObject.SetActive(true);
        trustVal.gameObject.SetActive(true);
    }
    private void ActivateRewardB()
    {
        icon2.gameObject.SetActive(true);
        respect2.gameObject.SetActive(true);
        respectVal2.gameObject.SetActive(true);
        trust2.gameObject.SetActive(true);
        trustVal2.gameObject.SetActive(true);
    }
    public void UpdateRewardUI(int _character, int _respect, int _trust)
    {     
        ResetUI();
        ActivateRewardA();    
        switch (_character)
        {
            case 0:
                icon1.sprite = ogun;
                icon1Shadow.sprite = ogun;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 1:
                icon1.sprite = dante;
                icon1Shadow.sprite = dante;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 2:
                icon1.sprite = turretta;
                icon1Shadow.sprite = turretta;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 3:
                icon1.sprite = galactica;
                icon1Shadow.sprite = galactica;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 4:
                icon1.sprite = karma;
                icon1Shadow.sprite = karma;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
        }

    }
    public void UpdateRewardUI(int _character, int _respect, int _trust, int _character2, int _respect2, int _trust2)
    {
        ResetUI();
        ActivateRewardA();
        ActivateRewardB();
        switch (_character)
        {
            case 0:
                icon1.sprite = ogun;
                icon1Shadow.sprite = ogun;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 1:
                icon1.sprite = dante;
                icon1Shadow.sprite = dante;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 2:
                icon1.sprite = turretta;
                icon1Shadow.sprite = turretta;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 3:
                icon1.sprite = galactica;
                icon1Shadow.sprite = galactica;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
            case 4:
                icon1.sprite = karma;
                icon1Shadow.sprite = karma;
                respectVal.text = ConvertRewardToString(_respect, respect);
                trustVal.text = ConvertRewardToString(_trust, trust);
                break;
        }

        switch (_character2)
        {
            case 0:
                icon2.sprite = ogun;
                icon2Shadow.sprite = ogun;
                respectVal2.text = ConvertRewardToString(_respect, respect2);
                trustVal2.text = ConvertRewardToString(_trust, trust2);
                break;
            case 1:
                icon2.sprite = dante;
                icon2Shadow.sprite = dante;
                respectVal2.text = ConvertRewardToString(_respect, respect2);
                trustVal2.text = ConvertRewardToString(_trust, trust2);
                break;
            case 2:
                icon2.sprite = turretta;
                icon2Shadow.sprite = turretta;
                respectVal2.text = ConvertRewardToString(_respect, respect2);
                trustVal2.text = ConvertRewardToString(_trust, trust2);
                break;
            case 3:
                icon2.sprite = galactica;
                icon2Shadow.sprite = galactica;
                respectVal2.text = ConvertRewardToString(_respect, respect2);
                trustVal2.text = ConvertRewardToString(_trust, trust2);
                break;
            case 4:
                icon2.sprite = karma;
                icon2Shadow.sprite = karma;
                respectVal2.text = ConvertRewardToString(_respect, respect2);
                trustVal2.text = ConvertRewardToString(_trust, trust2);
                break;
        }
    }

}
