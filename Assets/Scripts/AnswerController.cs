﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;

//Todo: move over to messaging system (unityevents or c# events) for updating buttons
public class AnswerController : MonoBehaviour
{
    public static AnswerController Instance;

    public List<Button> buttons;

    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogWarning("Finding answer controller. Ensure there is a game manager in the starting scene and in no other scenes.");
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }

        foreach (Button item in buttons)
        {
            item.gameObject.SetActive(false);
        }
    }

    public void ToggleActiveButtons(int number)
    {
        for (int i = 0; i < buttons.Count && i < number; i++)
        {
            buttons[i].gameObject.SetActive(!buttons[i].IsActive());
        }
    }
    
    public void ChangeButtonText(int button, string text)
    {
        buttons[button].transform.GetChild(0).GetComponent<Text>().text = text;
    }
    
    /// <summary>
    /// Button on the scene calls this function OnClick()
    /// </summary>
    /// <remarks>
    /// OnAnswer->ProcessAnswer->ChooseAnswer
    /// OnAnswer:      passes button pressed (buttons is a GameObject, int keypair)
    /// ProcessAnswer: calls ChooseAnswer on the state machine
    /// ChooseAnswer:  switches states on the state machine
    /// </remarks>
    /// <param name="obj"></param>
    public void OnAnswer(Button obj)
    {
        GameManager.Instance.queuedButton.Enqueue(buttons.IndexOf(obj));
    }
}
