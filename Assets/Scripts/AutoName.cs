﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AutoName : MonoBehaviour
{
#if UNITY_EDITOR
    public bool autoName;
    public SpriteRenderer rend;

    private void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
        if (autoName && rend.sprite != null)
        {
            this.gameObject.name = rend.sprite.name;
        }
    }
#endif
}
