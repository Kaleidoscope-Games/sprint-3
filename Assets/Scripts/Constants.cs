﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants {
    
    public const float ANSWER_BUTTON_SIZE = 100F;
    public const float ANSWER_BUTTON_OFFSET = 50F;
    public const string ANIMATOR_TOGGLE_NAME = "Toggle";
    public const string DIALOGUE_REGEX =
        @"(?<TextPrior>[\w\d\s….,;:'""‘’?!*-]*)(?<MarkupInner><[\w\d=#]*>)*(?<MarkupText>[\w\d\s.…,;:'""‘’?!*-]*)(?<MarkupOuter><\/[\w]*>)*(?<TextAfter>[\w\d\s.…,;:'""‘’?!*-]*)";
    public const string ASSET_REGEX =
        @"(?<Speaker>^[^:]+)*[\s]*:[\s]*(?<Dialogue>.*)";
}
