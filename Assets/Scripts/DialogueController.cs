﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

//Todo: fix bug where starting with no space on dialogue and markup will display markup (":<i>text</i>")
//Todo: move regex strings to constants
public class DialogueController : MonoBehaviour {
    private enum RegexGroups
    {
        TextPrior = 1,
        MarkupInner,
        MarkupText,
        MarkupOuter,
        TextAfter,
    }
    public static DialogueController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogWarning("Finding dialogue controller. Ensure there is a game manager in the starting scene and in no other scenes.");
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public Text nameTextField;
    public Text dialogueTextField;
    public float textSpeed = 0.1f;

    public void ReadSentence(string sentence, string speakerName/*, Animator _speakerAnim, Sprite _speakerImage, AudioClip _speakerNoise*/)
    {
        StopAllCoroutines();
        var matches = Regex.Matches(sentence,
            @"(?<TextPrior>[\w\d\s….,;:'""‘’?!*-]*)(?<MarkupInner><[\w\d=#]*>)*(?<MarkupText>[\w\d\s.…,;:'""‘’?!*-]*)(?<MarkupOuter><\/[\w]*>)*(?<TextAfter>[\w\d\s.…,;:'""‘’?!*-]*)"
            );
        nameTextField.text = speakerName;
        StartCoroutine(TypeSentence(matches, sentence.Length));
    }
    
    IEnumerator TypeSentence (MatchCollection collections, int length)
    {
        dialogueTextField.text = String.Empty;
        int insertionPos = 0;
        
        for (int i = 0; i < collections.Count; i++)
        { //for each collection
            var collection = collections[i];
            for (int group = 1; group <= collection.Groups.Count; group++)
            { //for each group in collection
                var stringGroup = collection.Groups[group].Value; //.ToCharArray() redundant: it does this automatically
                switch (group)
                {
                    case (int) RegexGroups.MarkupInner: 
                        //get current position of printed text + length of inner markup for insertion
                        // "blahblah <i>X ..."
                        insertionPos = dialogueTextField.text.Length + collection.Groups["MarkupInner"].Length;
                        //put both markups in textfield
                        // "blahblah <i></i> ..."
                        dialogueTextField.text += collection.Groups["MarkupInner"].Value + collection.Groups["MarkupOuter"].Value;
                        break;
                    
                    case (int) RegexGroups.MarkupText:
                        for (int j = 0; j < stringGroup.Length; j++)
                        { //for each letter in each group in each collection
                            //insert between markup
                            // "blahblah <i>X</i> ..."
                            dialogueTextField.text = dialogueTextField.text.Insert(insertionPos + j, stringGroup[j].ToString());
                            yield return new WaitForSeconds(textSpeed);
                        }
                        break;
                    
                    case (int) RegexGroups.MarkupOuter:
                        continue;
                    
                    default:
                        foreach (var letter in stringGroup)
                        { //for each letter in each group in each collection
                            //append text
                            // "blahblah ..."
                            dialogueTextField.text += letter;
                            yield return new WaitForSeconds(textSpeed);
                        }
                        break;
                }
            }
        }
    }
}
