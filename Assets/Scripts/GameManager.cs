﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using Assets.Scripts.Tools.Editor; //change the name of this namespace
using System;

//Todo: create default constructor for characters so that it's cleaner
//Todo: store dialogue choices
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public enum NPCName
    {
        Ogun, Dante, Turretta, Galactica, Karma, Villain
    }
    public struct NPCharacter
    {
        public NPCName npcName;
        [Range(-100, 100)]
        public int respectValue;
        [Range(-100, 100)]
        public int trustValue;
        public Sprite[] sprites;
    }

    //Add player struct for player customization

    private int convoNumber;
    private NPCharacter Ogun, Dante, Turretta, Galactica, Karma, Villain;
    public Color uiColor1, uiColor2;

    public StateMachine StateMachine;

    public enum PlayerPower { Gadgeteer, Archer, Boxer, Acrobat};
    public PlayerPower playerPower;
    public string playerName;

    public List<string> pastNodeIDs { get; private set; }

    private EditorNodeTree nodeTree;
    public List<string> ScenePaths; //{ private set; get; }
    public int CurrentScene { private set; get; }
    public string CurrentNode { private set; get; }
    public Node CurrentNodeObject { private set; get; }

    /// <summary>
    /// Dirty way of handling answer buttons.
    /// </summary>
    public Queue<int> queuedButton = new Queue<int>(1);

    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogWarning("Finding game manager. Ensure there is a game manager in the starting scene and in no other scenes.");
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
        
        DontDestroyOnLoad(gameObject);

        StateMachine = new StateMachine();
        //ScenePaths = new List<string>();
        //{
        //    "Assets/Resources/Act1/Intro.asset",
        //    "",
        //    "", };
        CurrentScene = 0;
    }

    public void RememberChoice(string node)
    {

    }

    //public bool CheckCondition(int character, bool isRespect)
    //{
    //    //switch statement to different characters
    //    return true;
    //}

    //public bool CheckCondition(int powerType)
    //{
    //    if ((int)playerPower == powerType)
    //    {
    //        return true;
    //    }
    //    else
    //    {
    //        return false;
    //    }
    //}

    private void Start()
    {
        pastNodeIDs = new List<string>();
        StateMachine.ChangeState(LinkNodes(ScenePaths[0]));
        PanelController.Instance.ToggleDialoguePanel();
        //Ogun
        Ogun = new NPCharacter();
        Ogun.npcName = NPCName.Ogun;
        Ogun.respectValue = 0;
        Ogun.trustValue = 0;
        Ogun.sprites = new Sprite[6];
        //Dante
        Dante = new NPCharacter();
        Dante.npcName = NPCName.Dante;
        Dante.respectValue = 0;
        Dante.trustValue = 0;
        Dante.sprites = new Sprite[6];
        //Turretta
        Turretta = new NPCharacter();
        Turretta.npcName = NPCName.Turretta;
        Turretta.respectValue = 0;
        Turretta.trustValue = 0;
        Turretta.sprites = new Sprite[6];
        //Galactica
        Galactica = new NPCharacter();
        Galactica.npcName = NPCName.Galactica;
        Galactica.respectValue = 0;
        Galactica.trustValue = 0;
        Galactica.sprites = new Sprite[6];
        //Karma
        Karma = new NPCharacter();
        Karma.npcName = NPCName.Karma;
        Karma.respectValue = 0;
        Karma.trustValue = 0;
        Karma.sprites = new Sprite[6];
        //Villain
        Villain = new NPCharacter();
        Villain.npcName = NPCName.Villain;
        Villain.respectValue = 0;
        Villain.trustValue = 0;
        Villain.sprites = new Sprite[6];
    }

    /// <summary>
    /// Change the trust and respect value for a particular NPC
    /// </summary>
    /// <param name="_npcName">Name of the npc (spelling mistakes will result in errors)</param>
    /// <param name="trustValue">How much trust to add. Set to negative to decrease.</param>
    /// <param name="respectValue">How much respect to add. Set to negative to decrease.</param>
    public void ChangeNPCAttitude(NPCName npcName, int trustValue, int respectValue)
    {
        //Debug.LogFormat("Setting {2}'s affinity! {0}, {1}", trustValue, respectValue, npcName);

        switch (npcName)
        {
            case NPCName.Ogun:
                Ogun.trustValue += trustValue;
                Ogun.respectValue += respectValue;
                break;
            case NPCName.Dante:
                Dante.trustValue += trustValue;
                Dante.respectValue += respectValue;
                break;
            case NPCName.Turretta:
                Turretta.trustValue += trustValue;
                Turretta.respectValue += respectValue;
                break;
            case NPCName.Galactica:
                Galactica.trustValue += trustValue;
                Galactica.respectValue += respectValue;
                break;
            case NPCName.Karma:
                Karma.trustValue += trustValue;
                Karma.respectValue += respectValue;
                break;
            case NPCName.Villain:
                Villain.trustValue += trustValue;
                Villain.respectValue += respectValue;
                break;
        }
    }

    private void Update()
    {
        StateMachine.Update();
    }


    public void Reward(int character, int trust , int respect)
    {
        //Debug.Log("Rewarding player");
        switch (character)
        {
            case 0:
                ChangeNPCAttitude(NPCName.Ogun, trust, respect);
                break;
            case 1:
                ChangeNPCAttitude(NPCName.Dante, trust, respect);
                break;
            case 2:
                ChangeNPCAttitude(NPCName.Turretta, trust, respect);
                break;
            case 3:
                ChangeNPCAttitude(NPCName.Galactica, trust, respect);
                break;
            case 4:
                ChangeNPCAttitude(NPCName.Karma, trust, respect);
                break;
        }
        PanelController.Instance.rewardUI.UpdateRewardUI(character, respect, trust);
    }
    public void Reward(int character, int trust , int respect, int character2, int trust2, int respect2)
    {
        switch (character)
        {
            case 0:
                ChangeNPCAttitude(NPCName.Ogun, trust, respect);
                break;
            case 1:
                ChangeNPCAttitude(NPCName.Dante, trust, respect);
                break;
            case 2:
                ChangeNPCAttitude(NPCName.Turretta, trust, respect);
                break;
            case 3:
                ChangeNPCAttitude(NPCName.Galactica, trust, respect);
                break;
            case 4:
                ChangeNPCAttitude(NPCName.Karma, trust, respect);
                break;
        }
        switch (character2)
        {
            case 0:
                ChangeNPCAttitude(NPCName.Ogun, trust2, respect2);
                break;
            case 1:
                ChangeNPCAttitude(NPCName.Dante, trust2, respect2);
                break;
            case 2:
                ChangeNPCAttitude(NPCName.Turretta, trust2, respect2);
                break;
            case 3:
                ChangeNPCAttitude(NPCName.Galactica, trust2, respect2);
                break;
            case 4:
                ChangeNPCAttitude(NPCName.Karma, trust2, respect2);
                break;
        }
        PanelController.Instance.rewardUI.UpdateRewardUI(character, respect, trust, character2, respect2, trust2);
    }

    public bool CheckClass(int power)
    {
        return power == (int)playerPower;
    }

    public void AddUsedNodeID(string nodeId)
    {
        pastNodeIDs.Add(nodeId);
    }
    /// <summary>
    /// Checks if current trust of character is greater or equal than the threshold.
    /// </summary>
    /// <param name="character"></param>
    /// <param name="threshold"></param>
    /// <returns></returns>
    public bool CheckTrust(int character, int threshold)
    {
        var currentValue = 0;
        if (character.Equals(0)) currentValue = Ogun.trustValue;
        else if (character.Equals(1)) currentValue = Dante.trustValue;
        else if (character.Equals(2)) currentValue = Turretta.trustValue;
        else if (character.Equals(3)) currentValue = Galactica.trustValue;
        else if (character.Equals(4)) currentValue = Karma.trustValue;
        //else if (character.Equals(Villain.npcName)) currentValue = Villain.trustValue;

        return currentValue >= threshold;
    }

    /// <summary>
    /// Checks if current respect of character is greater or equal than the threshold.
    /// </summary>
    /// <param name="character"></param>
    /// <param name="threshold"></param>
    /// <returns></returns>
    public bool CheckRespect(int character, int threshold)
    {
        var currentValue = 0;
        if (character.Equals(0)) currentValue = Ogun.respectValue;
        else if (character.Equals(1)) currentValue = Dante.respectValue;
        else if (character.Equals(2)) currentValue = Turretta.respectValue;
        else if (character.Equals(3)) currentValue = Galactica.respectValue;
        else if (character.Equals(4)) currentValue = Karma.respectValue;
        //else if (character.Equals(Villain.npcName)) currentValue = Villain.respectValue;

        return currentValue >= threshold;
    }

    /// <summary>
    /// Loads asset file from path and goes through each node, linking them to one another.
    /// </summary>
    /// <param name="assetPath">E.g. "Assets/Resources/Dump..."</param>
    public IState LinkNodes(string assetPath)
    {
        nodeTree = UnityEditor.AssetDatabase.LoadAssetAtPath<EditorNodeTree>(assetPath);
        IState start = null;
        for (int i = 0; i < nodeTree.nodes.Count; i++)
        {
            if (nodeTree.nodes[i].GetType() == typeof(EntryNode))
                start = (IState)nodeTree.nodes[i];
            nodeTree.nodes[i].nextNodes = new List<Node>();
            for (int j = 0; j < nodeTree.nodes.Count; j++)
            {
                if (nodeTree.nodes[i].serializableNextNodes.Contains(nodeTree.nodes[j].GUID))
                {
                    nodeTree.nodes[i].nextNodes.Add(nodeTree.nodes[j]);
                }
            }
        }
        if (start == null)
            Debug.LogError("Error: Either the start node doesn't exist in this automata or this function failed to find it.");
        return start;
    }

    public void LoadNextScene(int scene = 0)
    {
        CurrentScene++;
        if (CurrentScene == ScenePaths.Count)
        {   //do something at the end of the game
            CurrentScene = scene;
        }
        var newSt = LinkNodes(ScenePaths[CurrentScene]);
        StateMachine.ChangeState(newSt);
    }

    public void UpdateCurrentNode(string GUID, Node node)
    {
        CurrentNode = GUID;
        CurrentNodeObject = node;
    }
}
