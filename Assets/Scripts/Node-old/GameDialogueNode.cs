﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

//[System.Serializable]
public class GameDialogueNode : GameNode, INode, IState
{
    public Character character;

    public GameDialogueNode(string GUID, string speaker, string dialogue) : base(GUID)
    {
        character = new Character();
        character.characterName = speaker;
        character.characterText = dialogue;
    }

    void IState.Enter()
    {
        throw new System.NotImplementedException();
    }

    void IState.Exit()
    {
        throw new System.NotImplementedException();
    }

    void IState.FixedUpdate()
    {
        throw new System.NotImplementedException();
    }

    void IState.Update()
    {
        throw new System.NotImplementedException();
    }
}
