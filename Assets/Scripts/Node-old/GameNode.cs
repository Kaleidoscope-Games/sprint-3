﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Literal storage containers for nodes
/// </summary>
/// <remarks>
/// Todo: write custom serializer instead of having to constantly use scriptable objects (this is done to preserve class inheritence)
///     either do the above or instead make individual classes inheriting same interface; abstract/virtual classes still have inheritence problem -- there is no support for polymorphism/inheritance
///     Below is a thread discussing every solution I could think of:
///     https://www.reddit.com/r/gamedev/comments/6wadff/how_do_you_use_polymorphism_in_unity_3d/
///     I've done a weird jank of every solution and scriptable objects and serializable data classes (and then rebuilding them) are the cleanest options.
///     Scriptable objects are easier to do. In official builds, I would work on storing them in serializable data sets, as it seems to be the 'official' recommended solution
///     https://docs.unity3d.com/Manual/script-Serialization-Custom.html
///     Although it still doesn't solve the polymorphism issue.
///     
/// The most ideal way to do these would be to merge this class with the base node class used in the editor and it's corresponding subclasses.
/// Make the editor versions carry the state interface. Terribly storage inefficient, but it looks cleaner.
/// </remarks>
[System.Serializable]
public class GameNode : INode {
    [System.Serializable]
    public struct Character
    {
        //public Sprite characterSprite;
        //public string characterSpritePath;
        //so one of three things can happen here: 
        //-serialize just the character name and text and then use those during the game to look up appropriate sprites (preloaded in memory/stored on game manager/on a monobehavior)
        //-serialize sprite path and then load it on deserialization (not recommended apparently for auto atlasing conflicts)
        //-use a custom serializer to serialize sprites
        //I picked the first option because it's easier and I just need this demo to run; we'll do code clean up on all of this after the event, probably
        public string characterName;
        public string characterText;
    }

    [System.NonSerialized] public List<GameNode> nextNodes; //don't fiddle with this in editor
    public List<string> serializableNextNodes; //rebuild connections in game manager
    public string GUID;

    public GameNode(string GUID)
    {
        nextNodes = new List<GameNode>();
        serializableNextNodes = new List<string>();
        this.GUID = GUID;
    }

    public string GetGUID() { return GUID; }

    public GameNode GetNode() { return this; }

    public List<GameNode> GetNextNodes() { return nextNodes; }

    //public List<GameNode> GetPreviousNodes() { return prevNodes; }

    public override string ToString() { return GUID; }
}
