﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Obsolete
//Todo: remove from nodes
public interface INode {
    string GetGUID();
    GameNode GetNode();
    List<GameNode> GetNextNodes();
    //List<GameNode> GetPreviousNodes();
}
