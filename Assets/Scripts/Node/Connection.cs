﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Connections are the highest level in the heirarchy and hold information about in/out connectors
    /// and therefore by extension the node they connect as well
    /// </summary>
    [Serializable]
    public class Connection : ISerializationCallbackReceiver
    {
        public ConnectionPoint InPoint { private set; get; }
        public ConnectionPoint OutPoint { private set; get; }
        [SerializeField] private string _inPointGUID;
        public string InPointGUID { private set { _inPointGUID = value; }  get { return _inPointGUID; } }
        [SerializeField] private string _outPointGUID;
        public string OutPointGUID { private set { _outPointGUID = value; } get { return _outPointGUID; } }
        private int currentNodes;
        private int previousNodes;
        private int thisConnection;

        public Connection(ConnectionPoint inPoint, ConnectionPoint outPoint)
        {
            InPoint = inPoint;
            OutPoint = outPoint;
            InPointGUID = InPoint.node.GUID;
            OutPointGUID = OutPoint.node.GUID;
            previousNodes = 0;
            currentNodes = 0;
            thisConnection = 0;
        }

        /// <summary>
        /// Draws the red rectangle that is at the center of each connection line
        /// </summary>
        public void Draw()
        {
            //the line
            Handles.DrawBezier(
                InPoint.rect.center,
                OutPoint.rect.center,
                InPoint.rect.center + Vector2.left * 50f,
                OutPoint.rect.center - Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            //number
            var nodes = OutPoint.node.serializableNextNodes;
            currentNodes = nodes.Count;
            if (currentNodes != previousNodes)
            {
                previousNodes = currentNodes;
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (nodes[i].Equals(InPointGUID))
                    {
                        thisConnection = i + 1;
                    }
                }
            }
            if (currentNodes > 1)
            {
                Handles.Label((InPoint.rect.center + OutPoint.rect.center + OutPoint.rect.center/40) * 0.5f, thisConnection.ToString());
            }


            //the button
            Handles.color = Color.red;
            if (Handles.Button((InPoint.rect.center + OutPoint.rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
            {
                NodeEditor.OnClickRemoveConnection(this);
            }
        }

        /// <summary>
        /// Assigns the connection points (in/out) that this connection line is to follow
        /// 
        /// Used for serialization
        /// </summary>
        /// <param name="point"></param>
        public void Reconnect(ConnectionPoint point)
        {
            //Debug.Log(point.type);
            switch (point.type)
            {
                case ConnectionPointType.In:
                    InPoint = point;
                    break;
                case ConnectionPointType.Out:
                    OutPoint = point;
                    break;
            }
            //Debug.Log(inPoint);
        }

        #region Serialization
        public void OnBeforeSerialize()
        {
            try
            {
                InPointGUID = InPoint.node.GUID;
                OutPointGUID = OutPoint.node.GUID;
            }
            catch (NullReferenceException) { }

        }

        public void OnAfterDeserialize()
        {
            
        }
        #endregion
    }
}