﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Tools.Editor
{
    public enum ConnectionPointType { In, Out }

    /// <summary>
    /// Connection points are cyclic with nodes and come in pairs (typically)
    /// Each connection point is a single instance of this class
    /// </summary>
    [Serializable]
    public class ConnectionPoint : ISerializationCallbackReceiver
    {
        public Rect rect;
        public ConnectionPointType type;
        [NonSerialized] public Node node; //NonSerialized so that serialization is non-cyclic (non-infinite)
        [SerializeField] private GUIStyle style;
        [SerializeField] private string _nodeID;
        public string NodeID { get { return _nodeID; } private set { _nodeID = value; } }

        public ConnectionPoint(Node node, ConnectionPointType type)
        {
            this.node = node;
            this.type = type;
            switch (type)
            {
                case ConnectionPointType.In:
                    this.style = GUIStyles.InPoint;
                    break;
                case ConnectionPointType.Out:
                    this.style = GUIStyles.OutPoint;
                    break;
            }
            rect = new Rect(0, 0, 10f, 20f);
        }

        /// <summary>
        /// Draws the connection node and its location relative to the node it is connected to
        /// </summary>
        public void Draw()
        {
            //if (node == null) { Debug.Log("node null"); return; }
            //the connector itself
            //rect.y = node.rect.y + (node.rect.height * 0.5f) - rect.height * 0.5f;

            //switch (type)
            //{
            //    case ConnectionPointType.In:
            //        rect.x = node.rect.x - rect.width + 8f;
            //        break;
            //    case ConnectionPointType.Out:
            //        rect.x = node.rect.x + node.rect.width - 8f;
            //        break;
            //}

            //the button
            if (GUI.Button(rect, "", style))
            {
                switch (type)
                {
                    case ConnectionPointType.In:
                        NodeEditor.OnClickInPoint(this);
                        break;
                    case ConnectionPointType.Out:
                        NodeEditor.OnClickOutPoint(this);
                        break;
                }
            }
        }

        /// <summary>
        /// Assigns the node that this connection point is supposed to be connected to
        /// 
        /// Used mainly for serialization
        /// </summary>
        /// <param name="node"></param>
        public void Reconnect(Node node)
        {
            this.node = node;
        }

        #region Serialization
        public void OnBeforeSerialize()
        {
            try
            {
                NodeID = node.GUID;
            }
            catch (NullReferenceException) { }
        }

        public void OnAfterDeserialize()
        {

        }
        #endregion
    }
}