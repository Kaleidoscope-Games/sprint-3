﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Scriptable object used to encapsulate necessary information to draw and hold information about the node tree
    /// </summary>
    [SerializeField]
    public class EditorNodeTree : ScriptableObject, ISerializationCallbackReceiver
    {
        public List<Node> nodes;
        public List<Connection> connections;

        /// <summary>
        /// Initialization method that is basically a constructor method because Unity sucks
        /// </summary>
        public void Init(List<Node> nodes, List<Connection> connections)
        {
            this.nodes = nodes;
            this.connections = connections;
        }

        public void OnBeforeSerialize()
        {

        }

        public void OnAfterDeserialize()
        {

        }
    }
}
