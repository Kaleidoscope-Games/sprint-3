﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    [System.Serializable]
    public static class GUIStyles
    {
        public static GUIStyle Node = CreateNodeStyle();
        public static GUIStyle SelectedNode = CreateSelectedStyle();
        public static GUIStyle InPoint = CreateInPointStyle();
        public static GUIStyle OutPoint = CreateOutPointStyle();
        public static GUIStyle ButtonNormal = CreateButtonNormalStyle();

        public static GUIStyle CreateButtonNormalStyle()
        {
            return new GUIStyle(EditorStyles.miniButton);
        }

        public static GUIStyle CreateColoredLabel(Color color)
        {
            GUIStyle style = new GUIStyle(EditorStyles.label);
            style.normal.textColor = color;
            style.alignment = TextAnchor.MiddleCenter;
            return style;
        }
        private static GUIStyle CreateNodeStyle()
        {
            GUIStyle style = new GUIStyle();
            style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
            style.border = new RectOffset(12, 12, 12, 12);
            return style;
        }

        private static GUIStyle CreateSelectedStyle()
        {
            GUIStyle style = new GUIStyle();
            style = new GUIStyle();
            style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1 on.png") as Texture2D;
            style.border = new RectOffset(12, 12, 12, 12);
            return style;
        }

        private static GUIStyle CreateInPointStyle()
        {
            GUIStyle style = new GUIStyle();
            style = new GUIStyle();
            style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left.png") as Texture2D;
            style.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn left on.png") as Texture2D;
            style.border = new RectOffset(4, 4, 12, 12);
            return style;
        }

        private static GUIStyle CreateOutPointStyle()
        {
            GUIStyle style = new GUIStyle();
            style = new GUIStyle();
            style.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right.png") as Texture2D;
            style.active.background = EditorGUIUtility.Load("builtin skins/darkskin/images/btn right on.png") as Texture2D;
            style.border = new RectOffset(4, 4, 12, 12);
            return style;
        }
    }
}

