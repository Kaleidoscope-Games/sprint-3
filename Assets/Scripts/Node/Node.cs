﻿using System;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Base class for all node types.
    /// Is cyclic with ConnectionPoint class.
    /// </summary>
    /// <remarks>
    /// Unity serialization does not support inheritence and will only remember the true type
    /// if the base class is derived from scriptable object (presumably mono as well)
    /// 
    /// "No support for polymorphism.
    /// If you have a public Animal[] animals and you put in an instance of a Dog, a Cat and 
    /// a Giraffe, after serialization, you have three instances of Animal."
    /// Source: https://docs.unity3d.com/Manual/script-Serialization.html
    /// </remarks>
    [Serializable]
    public class Node : ScriptableObject
    {
        /// <summary>
        /// The Rect of the node
        /// </summary>
        public Rect rect;
        /// <summary>
        /// Checks that the node is being dragged
        /// </summary>
        [NonSerialized] public bool isDragged;
        /// <summary>
        /// Highlights the node for being selected
        /// </summary>
        [NonSerialized] public bool isSelected;
        /// <summary>
        /// Connection point for the connections coming into the node
        /// </summary>
        public ConnectionPoint inPoint;
        /// <summary>
        /// Connection point for the leaving the node
        /// </summary>
        public ConnectionPoint outPoint;
        /// <summary>
        /// Current style of the node
        /// </summary>
        [SerializeField] private GUIStyle currentStyle;
        /// <summary>
        /// Object's GUID assigned by System
        /// </summary>
        public string GUID;
        /// <summary>
        /// Identifier only used by the editor
        /// </summary>
        public string ID;
        /// <summary>
        /// Column width
        /// </summary>
        protected float columnWidth;
        /// <summary>
        /// Column width offset
        /// </summary>
        protected float columnOffset;
        /// <summary>
        /// Temporary buffer to store things from CSVViewer
        /// </summary>
        [NonSerialized] public string[] buffer;

        #region Game-specific
        [Serializable]
        public struct Character
        {
            public string characterName;
            public string characterText;
        }

        [System.NonSerialized] public List<Node> nextNodes; //don't fiddle with this in editor
        public List<string> serializableNextNodes; //rebuild connections in game manager
        #endregion

        public virtual void Init(Vector2 position, string GUID, string ID, float width = 200, float height = 50)
        {
            rect = new Rect(position.x, position.y, width, height);
            currentStyle = GUIStyles.Node;
            inPoint = new ConnectionPoint(this, ConnectionPointType.In);
            outPoint = new ConnectionPoint(this, ConnectionPointType.Out);
            this.GUID = GUID;
            this.ID = ID;

            serializableNextNodes = new List<string>();
        }

        /// <summary>
        /// Changes the rect of this object if it is selected and dragged
        /// </summary>
        /// <param name="delta"></param>
        public void Drag(Vector2 delta)
        {
            rect.position += delta;
        }

        /// <summary>
        /// Draws the connection points (this is the base that is called by the NodeEditor class)
        /// as well as itself. Subclasses add additional features and call this as their base
        /// </summary>
        public virtual void Draw()
        {
            columnWidth = CalculateWidth();
            columnOffset = CalcualteWidthOffset();

            //the connector itself
            if (GetType() != typeof(EntryNode))
            {
                inPoint.Draw();
                //Todo: below issue might've been fixed; possibly move back to connection point class
                inPoint.rect.y = rect.y + (rect.height * 0.5f) - inPoint.rect.height * 0.5f; //so this is okay here but not in ConnectionPoint? 
                //like the connection issue in NodeEditor there is seriously something wrong with how memory references are reassigned during serialization but not after
                inPoint.rect.x = rect.x - inPoint.rect.width + 8f;
            }
            if (GetType() != typeof(ExitNode))
            {
                outPoint.Draw();
                outPoint.rect.y = rect.y + (rect.height * 0.5f) - outPoint.rect.height * 0.5f;
                outPoint.rect.x = rect.x + rect.width - 8f;
            }

            GUI.BeginGroup(rect, currentStyle);
            GUI.EndGroup();
        }

        /// <summary>
        /// Process mouse events when this node is selected
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public bool ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        if (rect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                            isSelected = true;
                            currentStyle = GUIStyles.SelectedNode;
                        }
                        else
                        {
                            GUI.changed = true;
                            isSelected = false;
                            currentStyle = GUIStyles.Node;
                        }
                    }
                    if (e.button == 1 && isSelected && rect.Contains(e.mousePosition))
                    {
                        ProcessContextMenu();
                        e.Use();
                    }
                    break;

                case EventType.MouseUp:
                    isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            }

            return false;
        }

        /// <summary>
        /// Processes context menu on right click
        /// </summary>
        protected void ProcessContextMenu()
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Remove node"), false, OnClickRemoveNode);
            genericMenu.ShowAsContext();
        }

        /// <summary>
        /// Deletes this node and all connections relevant to this node
        /// </summary>
        protected void OnClickRemoveNode()
        {
            NodeEditor.OnClickRemoveNode(this);
        }

        /// <summary>
        /// Returns the height of this node
        /// </summary>
        /// <param name="row"></param>
        /// <param name="rowHeight"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        protected float CalculateHeight(int row, float rowHeight, float offset = 7)
        {
            return rect.position.y + row * rowHeight + offset;
        }

        /// <summary>
        /// Returns the width of this node
        /// </summary>
        /// <param name="scaleOffset"></param>
        /// <returns></returns>
        protected float CalculateWidth(float scaleOffset = 20)
        {
            return rect.width - scaleOffset;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset"></param>
        /// <returns></returns>
        protected float CalcualteWidthOffset(float offset = 10)
        {
            return rect.position.x + offset;
        }

        public override string ToString()
        {
            return GUID;
        }
    }
}