﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Answer node
    /// </summary>
    //Todo: finish serialization
    [Serializable]
    public class AnswerNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 180F;
        private const float WIDTH = 300F;
        [SerializeField] private string filePath = "";
        [SerializeField] private List<string> answers;
        private int update;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
            answers = new List<string>(5) { "", "", "", "", "" };
            if (NodeEditor.defaultDatabase != "")
                filePath = NodeEditor.defaultDatabase;
        }

        public override void Draw()
        {
            if (buffer != null)
            {
                answers[update] = buffer[1];
                EditorWindow.FocusWindowIfItsOpen<NodeEditor>();
                EditorWindow.GetWindow<NodeEditor>().Repaint();
                buffer = null;
            }
            base.Draw();
            int h = 0;
            //Row 0 ---
            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(h, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Answer Node " + ID, GUIStyles.CreateColoredLabel(Color.white));
            //Row 1 ---
            h = 1;
            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(h, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),
                "GUID:", GUIStyles.CreateColoredLabel(Color.white));
            GUID = EditorGUI.TextField(new Rect(columnOffset + 1 * columnWidth / 4, CalculateHeight(h, ROW_HEIGHT), 3 * columnWidth / 4, ROW_HEIGHT), GUID);
            //Row 2 ---
            h = 2;
            if (GUI.Button(new Rect(columnOffset, CalculateHeight(h, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), "Select text database"))
            {
                filePath = EditorUtility.OpenFilePanel("Select text database", Application.dataPath + "/Resources", "csv");
            }
            filePath = EditorGUI.TextField(new Rect(columnOffset + columnWidth / 2, CalculateHeight(h, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), filePath);
            //Row 3 ---
            if (filePath != "")
            {
                h = 3;
                for (int i = 0; i < answers.Count; i++)
                {
                    //Debug.Log(update);
                    EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(h + i, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),
                    "Answer" + (i + 1), GUIStyles.CreateColoredLabel(Color.white));
                    if (GUI.Button(new Rect(columnOffset + 1 * columnWidth / 4, CalculateHeight(h + i, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), "Select"))
                    {
                        CSVViewer.OpenWindow();
                        var win = EditorWindow.GetWindow<CSVViewer>();
                        win.node = this;
                        win.path = filePath;
                        update = i;
                    }
                    answers[i] = EditorGUI.TextField(new Rect(columnOffset + columnWidth / 2, CalculateHeight(h + i, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), answers[i]);

                }
            }
        }

        #region State Machine
        public void Enter()
        {
            GameManager.Instance.UpdateCurrentNode(GUID, this);
            for (int i = 0; i < answers.Count; i++)
            {
                AnswerController.Instance.ChangeButtonText(i, answers[i]);
            }
            AnswerController.Instance.ToggleActiveButtons(nextNodes.Count);
        }

        public void Update()
        {
            if (GameManager.Instance.queuedButton.Count != 0)
            {
                GameManager.Instance.StateMachine.ChangeState(
                    (IState) nextNodes[GameManager.Instance.queuedButton.Dequeue()]);
            }
        }
        public void FixedUpdate()
        {
            
        }

        public void Exit()
        {
            AnswerController.Instance.ToggleActiveButtons(nextNodes.Count);
        }
        #endregion
    }
}
