﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Condition node
    /// </summary>
    [Serializable]
    public class ConditionNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 150F;
        private const float WIDTH = 300F;

        [SerializeField] private bool relationship;
        [SerializeField] private enum rCharacter { Ogun, Dante, Turretta, Galactica, Karma };
        [SerializeField] private rCharacter relationChar;
        [SerializeField] private int rCharIndex;

        [SerializeField] private bool trust;
        [SerializeField] private string rInput;
        [SerializeField] private int rValue = 0;

        [SerializeField] private bool power;
        [SerializeField] private enum Power { Gadgeteer, Archer, Boxer, Acrobat }
        [SerializeField] private Power powerChoice;
        private int powerIndex;

        [SerializeField] private bool choice;
        [SerializeField] private string nodeID;

        private string[] characters = new string[5] { "Ogun", "Dante", "Turretta", "Galactica", "Karma" };
        private string[] powers = new string[4] { "Gadgeteer", "Archer", "Boxer", "Acrobat" };
        public bool passCondition = false;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();

            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Condition: " + ID, GUIStyles.CreateColoredLabel(Color.white));

            relationship = GUI.Toggle(new Rect(columnOffset, CalculateHeight(2, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT),
                relationship, "Relationship");
            power = GUI.Toggle(new Rect(columnOffset, CalculateHeight(3, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), power, "Power");
            choice = GUI.Toggle(new Rect(columnOffset, CalculateHeight(4, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), choice, "Choice");

            if (relationship && !power && !choice)
            {
                //Relationship condition

                rCharIndex = EditorGUI.Popup(new Rect(columnOffset + columnWidth / 2, CalculateHeight(2, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT)
                    , rCharIndex, characters);
                relationChar = (rCharacter)rCharIndex;

                trust = GUI.Toggle(new Rect(columnOffset + columnWidth / 2, CalculateHeight(3, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), 
                    trust, "Trust:T, Like:F");
                rInput = EditorGUI.TextField(new Rect(columnOffset + columnWidth / 2, CalculateHeight(4, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT)
                    , rInput);
                if (rInput != null)
                    rValue = int.Parse(rInput, 0);

            }
            else if (power && !relationship && !choice)
            {
                //power condition
                powerIndex = EditorGUI.Popup(new Rect(columnOffset + columnWidth / 2, CalculateHeight(3, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT)
                    , powerIndex, powers);
                powerChoice = (Power)powerIndex;
            }
            else if (choice && !relationship && !power)
            {
                //Choice condition
                nodeID = EditorGUI.TextField(new Rect(columnOffset + columnWidth / 2, CalculateHeight(2, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT)
                    , nodeID);
            }
            else
            {
                //Display Error
                GUI.Label(new Rect(columnOffset + columnWidth / 2, CalculateHeight(3, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), "Choose one condition");
            }
        }

        public void Enter()
        {
            passCondition = false;
            GameManager gm = GameManager.Instance;
            if (choice)
            {
                passCondition = gm.pastNodeIDs.Contains(nodeID);
            }
            if (relationship)
            {
                if (trust)
                {
                    passCondition = gm.CheckTrust((int)relationChar, rValue);
                }
                else
                {
                    passCondition = gm.CheckRespect((int)relationChar, rValue);
                }
            }
            if (power)
            {
                passCondition = gm.CheckClass((int)powerChoice);
            }
        }

        public void Update()
        {
            if (passCondition)
            {
                GameManager.Instance.StateMachine.ChangeState((IState)this.nextNodes[0]);
            }
            else
            {
                GameManager.Instance.StateMachine.ChangeState((IState)this.nextNodes[1]);
            }
        }

        public void FixedUpdate()
        {
            //throw new NotImplementedException();
        }

        public void Exit()
        {
            //throw new NotImplementedException();
        }
    }
}
