﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    public class DecisionNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 100F;
        private const float WIDTH = 300F;
        [SerializeField] private string nodeID;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();
            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Decision: " + ID, GUIStyles.CreateColoredLabel(Color.white));
            if (this.serializableNextNodes.Count == 0)
            {
                EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(1, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Out Slot not connected to a node", GUIStyles.CreateColoredLabel(Color.red));
            }
            else
            {
                nodeID = this.serializableNextNodes[0];
                EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(1, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                    "Remembering this Node ", GUIStyles.CreateColoredLabel(Color.white));
                EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(2, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                    nodeID , GUIStyles.CreateColoredLabel(Color.yellow));
            }
        }
        public void Enter()
        {
            //optionally add a "Dave will remember this" bs
            GameManager.Instance.AddUsedNodeID(nodeID);
        }

        public void Exit()
        {
            //throw new System.NotImplementedException();
        }

        public void FixedUpdate()
        {
            //throw new System.NotImplementedException();
        }

        public void Update()
        {
            GameManager.Instance.StateMachine.ChangeState((IState)this.nextNodes[0]);
        }
    }
}
