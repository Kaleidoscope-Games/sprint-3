﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Dialogue node
    /// </summary>
    [Serializable]
    public class DialogueNode : Node, IState//ISerializationCallbackReceiver
    {
        
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 150F;
        private const float WIDTH = 300F;
        [SerializeField] private string filePath = "";

        public Character character;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);

            character = new Character();
            character.characterName = "";
            character.characterText = "";

            if (NodeEditor.defaultDatabase != "")
                filePath = NodeEditor.defaultDatabase;
        }

        #region Editor
        public override void Draw()
        {
            base.Draw();
           
            if (buffer != null)
            {
                character.characterName = buffer[0];
                character.characterText = buffer[1];

                EditorWindow.FocusWindowIfItsOpen<NodeEditor>();
                EditorWindow.GetWindow<NodeEditor>().Repaint();
                buffer = null;
            }

            //Row 1 ---
            if (GUI.Button(new Rect(columnOffset, CalculateHeight(1, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), "Select text database"))
            {
                filePath = EditorUtility.OpenFilePanel("Select text database", Application.dataPath + "/Resources", "csv");
            }
            if (filePath != "")
            {
                if (GUI.Button(new Rect(columnOffset + columnWidth / 2, CalculateHeight(1, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT), "Select contents"))
                {
                    CSVViewer.OpenWindow();
                    var win = EditorWindow.GetWindow<CSVViewer>();
                    win.node = this;
                    win.path = filePath;
                }
            }
            //Row 0 ---
            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Dialogue Node " + ID, GUIStyles.CreateColoredLabel(Color.white));
            //Row 2 ---
            EditorGUI.TextField(new Rect(columnOffset, CalculateHeight(2, ROW_HEIGHT), columnWidth, ROW_HEIGHT), filePath);
            //Row 3 ---
            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(3, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),
                "Speaker:", GUIStyles.CreateColoredLabel(Color.white));
            character.characterName = EditorGUI.TextField(new Rect(columnOffset + columnWidth / 4, CalculateHeight(3, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), character.characterName);
            EditorGUI.LabelField(new Rect(columnOffset + 2 * columnWidth / 4, CalculateHeight(3, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),
                "GUID:", GUIStyles.CreateColoredLabel(Color.white));
            GUID = EditorGUI.TextField(new Rect(columnOffset + 3 * columnWidth / 4, CalculateHeight(3, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), GUID);
            //Row 4 ---
            EditorStyles.textArea.wordWrap = true;
            EditorGUI.TextArea(new Rect(columnOffset, CalculateHeight(4, ROW_HEIGHT), columnWidth, ROW_HEIGHT * 2 + .5F * ROW_HEIGHT), character.characterText, EditorStyles.textArea);
        }
        #endregion
        #region Serialization
        public void OnBeforeSerialize()
        {

        }

        public void OnAfterDeserialize()
        {
        }
        #endregion
        #region State Machine
        public void Enter()
        {
            GameManager.Instance.UpdateCurrentNode(GUID, this);
            DialogueController.Instance.ReadSentence(character.characterText, character.characterName);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKeyDown(KeyCode.Space))
            {
                GameManager.Instance.StateMachine.ChangeState((IState)nextNodes[0]);   
            }
        }

        public void FixedUpdate()
        {
            
        }

        public void Exit()
        {
            
        }
        #endregion
    }
}
