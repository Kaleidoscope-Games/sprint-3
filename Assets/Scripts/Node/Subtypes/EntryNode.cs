﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    public class EntryNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 50F;
        private const float WIDTH = 60F;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();

            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Enter", GUIStyles.CreateColoredLabel(Color.white));
        }
        #region State Machine
        public void Enter()
        {
            
        }

        public void Update()
        {
            GameManager.Instance.StateMachine.ChangeState((IState)nextNodes[0]);
        }

        public void FixedUpdate()
        {
            
        }

        public void Exit()
        {
            
        }
        #endregion
    }
}
