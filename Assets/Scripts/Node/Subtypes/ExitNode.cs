﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    public class ExitNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 50F;
        private const float WIDTH = 60F;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();

            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Exit", GUIStyles.CreateColoredLabel(Color.white));
        }

        public void Enter()
        {
            
        }

        public void Update()
        {
            GameManager.Instance.LoadNextScene();
        }

        public void FixedUpdate()
        {
            
        }

        public void Exit()
        {
            
        }
    }
}
