﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Panel node
    /// </summary>
    [Serializable]
    public class PanelNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 80F;
        private const float WIDTH = 60F;

        public bool answerPanel;
        public bool dialoguePanel;
        public bool relationPanel;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();
            answerPanel = GUI.Toggle(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), 2 * columnWidth/3, ROW_HEIGHT)
                , answerPanel, "A");
            dialoguePanel = GUI.Toggle(new Rect(columnOffset, CalculateHeight(1, ROW_HEIGHT), 2 * columnWidth/3, ROW_HEIGHT)
                , dialoguePanel, "D");
            relationPanel = GUI.Toggle(new Rect(columnOffset, CalculateHeight(2, ROW_HEIGHT), 2 * columnWidth/3, ROW_HEIGHT)
                , relationPanel, "R");
        }
        public void Enter()
        {
            PanelController pc;
            pc = PanelController.Instance; 
            pc.SetAnswersPanel(answerPanel);
            pc.SetDialoguePanel(dialoguePanel);
            if (relationPanel)
            {
                pc.TriggerRewardPanel();
            }
        }

        public void Exit()
        {
            
        }

        public void FixedUpdate()
        {
            //throw new NotImplementedException();
        }

        public void Update()
        {
            GameManager.Instance.StateMachine.ChangeState((IState)this.nextNodes[0]);
        }
    }
}
