﻿using System;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Condition node
    /// </summary>
    [Serializable]
    public class RewardNode : Node, IState
    {
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 150F;
        private const float WIDTH = 300F;

        [SerializeField] private bool twoRewards;

        [SerializeField] private enum rCharacter { Ogun, Dante, Turretta, Galactica, Karma };
        [SerializeField] private rCharacter relationChar, relationChar2;
        [SerializeField] private int rCharIndex, rCharIndex2;
        [SerializeField] private int trust, trust2;
        [SerializeField] private int respect, respect2;

        private int powerIndex;

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();

            EditorGUI.LabelField(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), columnWidth, ROW_HEIGHT),
                "Reward: " + ID, GUIStyles.CreateColoredLabel(Color.white));

            twoRewards = EditorGUI.Toggle(new Rect(columnOffset, CalculateHeight(1, ROW_HEIGHT), columnWidth, ROW_HEIGHT), "Two Rewards", twoRewards);

            //FirstReward
            string[] options = new string[5];
            options[0] = "Ogun";
            options[1] = "Dante";
            options[2] = "Turretta";
            options[3] = "Galactica";
            options[4] = "Karma";
            rCharIndex = EditorGUI.Popup(new Rect(columnOffset, CalculateHeight(2, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT)
                , rCharIndex, options);
            relationChar = (rCharacter)rCharIndex;

            respect = EditorGUI.IntField(new Rect(columnOffset + columnWidth / 4, CalculateHeight(2, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), respect);
            EditorGUI.LabelField(new Rect(columnOffset + columnWidth * 2 / 4, CalculateHeight(2, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), ": Respect", GUIStyles.CreateColoredLabel(Color.white));
            trust = EditorGUI.IntField(new Rect(columnOffset + columnWidth / 4, CalculateHeight(3, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),  trust);
            EditorGUI.LabelField(new Rect(columnOffset + columnWidth * 2 / 4, CalculateHeight(3, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), ": Trust", GUIStyles.CreateColoredLabel(Color.white));

            if (twoRewards)
            {
                //FirstReward
                string[] options2 = new string[5];
                options[0] = "Ogun";
                options[1] = "Dante";
                options[2] = "Turretta";
                options[3] = "Galactica";
                options[4] = "Karma";
                rCharIndex2 = EditorGUI.Popup(new Rect(columnOffset, CalculateHeight(4, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT)
                    , rCharIndex2, options);
                relationChar2 = (rCharacter)rCharIndex2;

                respect2 = EditorGUI.IntField(new Rect(columnOffset + columnWidth / 4, CalculateHeight(4, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),  respect2);
                EditorGUI.LabelField(new Rect(columnOffset + columnWidth * 2 / 4, CalculateHeight(4, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), ": Respect", GUIStyles.CreateColoredLabel(Color.white));
                trust2 = EditorGUI.IntField(new Rect(columnOffset + columnWidth / 4, CalculateHeight(5, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT),  trust2);
                EditorGUI.LabelField(new Rect(columnOffset + columnWidth * 2 / 4, CalculateHeight(5, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), ": Trust", GUIStyles.CreateColoredLabel(Color.white));
            }
        }
        public void Enter()
        {
            if (twoRewards)
            {
                GameManager.Instance.Reward((int)relationChar, trust, respect, (int)relationChar2, trust2, respect2);
            }
            else
            {
                GameManager.Instance.Reward((int)relationChar, trust, respect);
            }

        }

        public void Update()
        {
            GameManager.Instance.StateMachine.ChangeState((IState)this.nextNodes[0]);
        }

        public void FixedUpdate()
        {
            //throw new NotImplementedException();
        }

        public void Exit()
        {
            //throw new NotImplementedException();
        }
    }
}
