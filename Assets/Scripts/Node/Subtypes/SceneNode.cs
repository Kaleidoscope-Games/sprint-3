﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Condition node
    /// </summary>
    [Serializable]
    public class SceneNode : Node, IState
    {
        public enum Selection { RotateActors, RotateBackgrounds, MoveOutScene, MoveIntoScene, }
        private const float ROW_HEIGHT = 20F;
        private const float HEIGHT = 120F;
        private const float WIDTH = 250F;

        public string raAmount = "1";
        public int raAmountInt;
        public bool left;
        public bool background;
        public bool foreground;
        public bool changeSprite;
        public int emotion;
        public int position;
        public string refName;
        public int selection = 0;
        private string[] text = new string[] { "Rotate Actors", "Rotate Backgrounds", "Move Out Scene", "Move Into Scene" };
        private string[] emotions = new string[] { "Neutral", "Angry", "Happy", "Sad", "Surprise", };
        private string[] positions = new string[] { "Center", "Left", "Right" };

        public override void Init(Vector2 position, string GUID, string ID, float width = WIDTH, float height = HEIGHT)
        {
            base.Init(position, GUID, ID, WIDTH, HEIGHT);
        }

        public override void Draw()
        {
            base.Draw();
            selection = GUI.SelectionGrid(new Rect(columnOffset, CalculateHeight(0, ROW_HEIGHT), 3 * columnWidth / 5, ROW_HEIGHT * 4), selection, text, 1, EditorStyles.radioButton);
            if (selection == (int)Selection.RotateActors || selection == (int)Selection.RotateBackgrounds)
            {
                EditorGUI.LabelField(new Rect(columnOffset + columnWidth / 2, CalculateHeight(0, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), "Amount");
                raAmount = EditorGUI.TextField(new Rect(columnOffset + 3 * columnWidth / 4, CalculateHeight(0, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), raAmount);
                if (!int.TryParse(raAmount, out raAmountInt))
                    Debug.LogWarning( "SceneNode could not convert the input RotateAmount!" );
                left = GUI.Toggle(new Rect(columnOffset + 3 * columnWidth / 5, CalculateHeight(1, ROW_HEIGHT), 2 * columnWidth / 5, ROW_HEIGHT)
                    , left, "Rotate Left");

            }
            else if (selection == (int)Selection.MoveOutScene || selection == (int)Selection.MoveIntoScene)
            {
                EditorGUI.LabelField(new Rect(columnOffset + columnWidth / 2, CalculateHeight(0, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), "Name");
                refName = EditorGUI.TextField(new Rect(columnOffset + 3 * columnWidth / 4, CalculateHeight(0, ROW_HEIGHT), columnWidth / 4, ROW_HEIGHT), refName);

                left = GUI.Toggle(new Rect(columnOffset + 3 * columnWidth / 5, CalculateHeight(1, ROW_HEIGHT), 2 * columnWidth / 5, ROW_HEIGHT)
                    , left, "Move Left");
                background = GUI.Toggle(new Rect(columnOffset + 3 * columnWidth / 5, CalculateHeight(2, ROW_HEIGHT), 2 * columnWidth/5, ROW_HEIGHT)
                    , background, "Background");
                if (background) foreground = false;

                foreground = GUI.Toggle(new Rect(columnOffset + 3 * columnWidth / 5, CalculateHeight(3, ROW_HEIGHT), 2 * columnWidth / 5, ROW_HEIGHT)
                    , foreground, "Foreground");
                if (foreground) background = false;
            }

            changeSprite = GUI.Toggle(new Rect(columnOffset, CalculateHeight(4, ROW_HEIGHT), 3 * columnWidth / 5, ROW_HEIGHT)
                    , changeSprite, "Change Sprite");
            if (changeSprite)
            {
                emotion = EditorGUI.Popup(new Rect(columnOffset + columnWidth / 2, CalculateHeight(3, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT)
                    , emotion, emotions);
                position = EditorGUI.Popup(new Rect(columnOffset + columnWidth / 2, CalculateHeight(4, ROW_HEIGHT), columnWidth / 2, ROW_HEIGHT)
                    , position, positions);
            }
        }
        public void Enter()
        {
            var inst = SceneController.Instance;
            var l = left ? SceneController.Direction.Left : SceneController.Direction.Right;
            if (selection == (int)Selection.RotateActors)
                inst.RotateActors(raAmountInt, l);
            else if (selection == (int)Selection.RotateBackgrounds)
                inst.RotateBackgrounds(raAmountInt, l);
            else
            {
                var list = (background || foreground) ? inst.backgrounds : inst.actors;
                //foreground
                if (foreground && selection == (int)Selection.MoveOutScene)
                    inst.MoveOutForeground(list[refName]);
                else if (foreground && selection == (int)Selection.MoveIntoScene)
                    inst.MoveIntoForeground(list[refName]);
                //regular
                else if (selection == (int)Selection.MoveOutScene)
                    inst.MoveOutScene(list[refName], l, background);
                else if (selection == (int)Selection.MoveIntoScene)
                    inst.MoveIntoScene(list[refName], l, background);
            }
            if (changeSprite)
            {
                inst.ChangeActorState(emotions[emotion], position);
            }
        }

        public void Exit()
        {

        }

        public void FixedUpdate()
        {

        }

        public void Update()
        {
            GameManager.Instance.StateMachine.ChangeState((IState)nextNodes[0]);
        }
    }
}
