﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour{

    public static PanelController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogWarning("Finding panel controller. Ensure there is a game manager in the starting scene and in no other scenes.");
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public Animator dialoguePanel, answersPanel, rewardPanel;
    public RewardPanelUI rewardUI;

    public void ToggleAnswersPanel()
    {
        answersPanel.SetBool("active", !answersPanel.GetBool("active"));
    }

    public void ToggleDialoguePanel()
    {
        dialoguePanel.SetBool("active", !dialoguePanel.GetBool("active"));
    }

    public void TriggerRewardPanel()
    {
        rewardPanel.SetTrigger("active");
    }

    public void SetAnswersPanel(bool a)
    {
        answersPanel.SetBool("active", a);
    }

    public void SetDialoguePanel(bool a)
    {
        dialoguePanel.SetBool("active", a);
    }
}
