﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

    public static SceneController Instance;

    //Below is for adding things via inspector; we store all the assets in scene for the purposes of our demo
    public StringSceneObjectDictionary actorStore = StringSceneObjectDictionary.New<StringSceneObjectDictionary>();
    public Dictionary<string, SceneObject> actors { get { return actorStore.dictionary; } }
    public StringSceneObjectDictionary backgroundStore = StringSceneObjectDictionary.New<StringSceneObjectDictionary>();
    public Dictionary<string, SceneObject> backgrounds { get { return backgroundStore.dictionary; } }

    /// <summary>
    /// Actors currently on the "stage"
    /// </summary>
    public List<SceneObject> activeActors;
    public List<SceneObject> activeBackgrounds;
    public Transform[] positionRefs;
    public Transform[] backgroundRefs;
    public GameObject scenePrefab, backgroundPrefab;
    public enum PositionReference { Center, Left, Right, OffLeft, OffRight, }
    public enum BackgroundReference { Current, Foreground, Left, Right, Storage }
    public enum Direction { Left, Right }

    private void Awake()
    {
        if (Instance == null)
        {
            Debug.LogWarning("Finding scene controller. Ensure there is a game manager in the starting scene and in no other scenes.");
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }

        activeActors = new List<SceneObject>();
        activeBackgrounds = new List<SceneObject>();
    }

    public void MoveIntoScene(SceneObject obj, Direction left = Direction.Left, bool background = false)
    {
        if (!background)
        {
            if (left == Direction.Left)
                obj.MoveToPoint(positionRefs[(int)PositionReference.Left]);
            else
                obj.MoveToPoint(positionRefs[(int)PositionReference.Right]);
            activeActors.Add(obj);
        }
        else
        {
            if (left == Direction.Left)
                obj.MoveToPoint(backgroundRefs[(int)BackgroundReference.Left]);
            else
                obj.MoveToPoint(backgroundRefs[(int)BackgroundReference.Right]);
            activeBackgrounds.Add(obj);
        }
    }

    public void MoveIntoForeground(SceneObject obj)
    {
        obj.MoveToPoint(backgroundRefs[(int)BackgroundReference.Foreground]);
    }

    public void MoveOutForeground(SceneObject obj)
    {
        obj.MoveToPoint(backgroundRefs[(int)BackgroundReference.Storage]);
    }

    public void MoveOutScene(SceneObject obj, Direction left = Direction.Left, bool background = false)
    {
        if (!background)
        {
            if (left == Direction.Left)
                obj.MoveToPoint(positionRefs[(int)PositionReference.OffLeft]);
            else
                obj.MoveToPoint(positionRefs[(int)PositionReference.OffRight]);
            activeActors.Remove(obj);
        }
        else
        {
            obj.MoveToPoint(backgroundRefs[(int)BackgroundReference.Storage]);
            activeBackgrounds.Remove(obj);
        }

    }

    /// <summary>
    /// Cycles background with either left  or right
    /// </summary>
    /// <param name=""></param>
    /// <param name="left"></param>
    public void RotateBackgrounds(int amount, Direction left = Direction.Left)
    {
        if (left != Direction.Left)
        {
            foreach (SceneObject so in activeBackgrounds)
            {
                for (int i = 0; i < amount; i++)
                {
                    if (so.currentPoint.Equals(backgroundRefs[(int)BackgroundReference.Left]))
                        so.MoveToPoint(backgroundRefs[(int)BackgroundReference.Current]);
                    else if (so.currentPoint.Equals(backgroundRefs[(int)BackgroundReference.Current]))
                        so.MoveToPoint(backgroundRefs[(int)BackgroundReference.Right]);
                    else if (so.currentPoint.Equals(backgroundRefs[(int)BackgroundReference.Right]))
                        so.MoveToPoint(backgroundRefs[(int)BackgroundReference.Left]);
                }
            }
        }
        else
        {
            foreach (SceneObject so in activeBackgrounds)
            {
                for (int i = 0; i < amount; i++)
                {
                    if (so.currentPoint.Equals(backgroundRefs[(int)BackgroundReference.Left]))
                        so.MoveToPoint(backgroundRefs[(int)BackgroundReference.Right]);
                    else if (so.currentPoint.Equals(backgroundRefs[(int)BackgroundReference.Current]))
                        so.MoveToPoint(backgroundRefs[(int)BackgroundReference.Left]);
                    else if (so.currentPoint.Equals(backgroundRefs[(int)BackgroundReference.Right]))
                        so.MoveToPoint(backgroundRefs[(int)BackgroundReference.Current]);
                }
            }
        }
    }

    public void RotateActors(int amount, Direction left = Direction.Left)
    {
        if (left != Direction.Left)
        {
            foreach (SceneObject so in activeActors)
            {
                Debug.Log(activeActors.Count);
                Debug.Log(positionRefs[(int)PositionReference.Right]);
                Debug.Log(so.currentPoint);
                for (int i = 0; i < amount; i++)
                {
                    if (so.currentPoint.Equals(positionRefs[(int)PositionReference.Left]))
                        so.MoveToPoint(positionRefs[(int)PositionReference.Center]);
                    else if (so.currentPoint.Equals(positionRefs[(int)PositionReference.Center]))
                        so.MoveToPoint(positionRefs[(int)PositionReference.Right]);
                    else if (so.currentPoint.Equals(positionRefs[(int)PositionReference.Right]))
                        so.MoveToPoint(positionRefs[(int)PositionReference.Left]);
                }
            }
        }
        else
        {
            foreach (SceneObject so in activeActors)
            {
                for (int i = 0; i < amount; i++)
                {
                    if (so.currentPoint.Equals(positionRefs[(int)PositionReference.Left]))
                        so.MoveToPoint(positionRefs[(int)PositionReference.Right]);
                    else if (so.currentPoint.Equals(positionRefs[(int)PositionReference.Right]))
                        so.MoveToPoint(positionRefs[(int)PositionReference.Center]);
                    else if (so.currentPoint.Equals(positionRefs[(int)PositionReference.Center]))
                        so.MoveToPoint(positionRefs[(int)PositionReference.Left]);
                }
            }
        }
    }

    /// <summary>
    /// Not states as in state machine, but the emotion/status of the character.
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="state"></param>
    /// <param name=""></param>
    public void ChangeActorState(string state, int position = (int)PositionReference.Center)
    {
        SceneObject guy = null;
        foreach (SceneObject so in activeActors)
        {
            if (so.currentPoint.Equals(positionRefs[position]))
                guy = so;
        }
        var anim = guy.GetComponent<Animator>();
        foreach (AnimatorControllerParameter p in anim.parameters)
        {
            anim.SetBool(p.name, false);
        }
        anim.SetBool(state, true);
    }
}
