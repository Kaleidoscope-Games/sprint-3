﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorManager : MonoBehaviour
{
    /// <summary>
    /// The game manager, where the ui gets the current color values from
    /// </summary>
    private GameManager gm;
    /// <summary>
    /// If no text is passed in will automatically find an image on the object
    /// </summary>
    private Image image;
    /// <summary>
    /// Check if changing to the secondary color, leave false if acting as a primary color
    /// </summary>
    [SerializeField]
    private bool secondary;
    /// <summary>
    /// Pass in if managing the colors of a text file instead of an image
    /// </summary>
    [SerializeField]
    private Text text;
    /// <summary>
    /// Controls the alpha independently from the game manager
    /// </summary>
    [Range(0, 100)]
    public float colorAlpha;

    // Use this for initialization
    void Start()
    {
        gm = GameManager.Instance;
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        //If there is no text file manage image colors, otherwise manage text colors
        if (text == null)
        {
            UpdateColor();
        }
        else
        {
            UpdateTextColor();
        }

    }
    /// <summary>
    /// Updates the text color if it changes over a lerp
    /// </summary>
    private void UpdateTextColor()
    {
        if (secondary)
        {
            if (text.color.r != gm.uiColor2.r && text.color.g != gm.uiColor2.g && text.color.b != gm.uiColor2.b)
            {
                Color color = new Color(gm.uiColor2.r, gm.uiColor2.g, gm.uiColor2.b, colorAlpha / 100);
                text.color = Color.Lerp(text.color, color, 0.1f);
            }
        }
        else
        {
            if (text.color.r != gm.uiColor1.r && text.color.g != gm.uiColor1.g && text.color.b != gm.uiColor1.b)
            {
                Color color = new Color(gm.uiColor1.r, gm.uiColor1.g, gm.uiColor1.b, colorAlpha / 100);
                text.color = Color.Lerp(text.color, color, 0.1f);
            }
        }
    }
    /// <summary>
    /// Update the image color if it changes over a lerp
    /// </summary>
    private void UpdateColor()
    {
        if (secondary)
        {
            if (image.color.r != gm.uiColor2.r && image.color.g != gm.uiColor2.g && image.color.b != gm.uiColor2.b)
            {
                Color color = new Color(gm.uiColor2.r, gm.uiColor2.g, gm.uiColor2.b, colorAlpha / 100);
                image.color = Color.Lerp(image.color, color, 0.1f);
            }
        }
        else
        {
            if (image.color != gm.uiColor1 && image.color.g != gm.uiColor1.g && image.color.b != gm.uiColor1.b)
            {
                Color color = new Color(gm.uiColor1.r, gm.uiColor1.g, gm.uiColor1.b, colorAlpha / 100);
                image.color = Color.Lerp(image.color, color, 0.1f);
            }
        }
    }
}
