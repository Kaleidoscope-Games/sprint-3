﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : IGameState
{
    public List<Transform> sceneFigures;
    public List<Transform> sceneBackgrounds;

    /// <summary>
    /// Shift scene objects clockwise or counterclockwise
    /// </summary>
    /// <param name="sceneObjects">Whether you shift the sceneFigures or backgrounds</param>
    /// <param name="shiftClockwise">Shift Clockwise if scenefigures, shift left if background</param>
    public void ChangeSceneElements(List<Transform> sceneObjects, bool shiftClockwise)
    {

    }
    /// <summary>
    /// Move to point 
    /// </summary>
    /// <param name="sceneObject"></param>
    /// <param name="left"></param>
    public void RemoveSceneElement(Transform sceneObject, bool left)
    {

    }
    public void Read(string currentDialogue, string speakerName)
    {

    }
    public void Answer(List<string> answers)
    {

    }
    public void Reward(int respectChange, int trustChange)
    {

    }
    public void ExitScene()
    {

    }
    public void EnterScene()
    {

    }

    public void Pause()
    {

    }

    
}
