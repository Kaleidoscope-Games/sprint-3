﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameState
{
    /// <summary>
    /// Move around any objects in the scene view
    /// </summary>
    void ChangeSceneElements(List<Transform> sceneObjects, bool shiftClockwise);
    void RemoveSceneElement(Transform sceneObject, bool left);
    void Read(string currentDialogue, string speakerName);
    void Answer(List<string> answers);
    void Reward(int respectChange, int trustChange);
    void ExitScene();
    void EnterScene();
    void Pause();
}
