﻿using UnityEngine;

public interface IManageable
{
    /// <summary>
    /// Move to one of the preset anchor points in the scene
    /// </summary>
    /// <param name="point">The anchor point that this will move to</param>
    void MoveToPoint(Transform point);
    /// <summary>
    /// Move to a custom point in space
    /// </summary>
    /// <param name="point">A custom position for this transition</param>
    void MoveToPoint(Vector3 point);
    //Probably should change this to object pool instead of destroy.
    /// <summary>
    /// Move to one of the preset anchors (Preferably one of the anchors off-camera) Then Destroy the Object.
    /// </summary>
    /// <param name="point">The point the object must reach before destroying itself</param>
    void MoveToPointThenDestroy(Transform point);
    /// <summary>
    /// Change the current sprite to a new one 
    /// </summary>
    /// <param name="newSprite">The new image that will be applied to this object</param>
    void ChangeSprite(Sprite newSprite);


}
