﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneObject : MonoBehaviour, IManageable {

    //Add animation later??

    private SpriteRenderer rend;
    private Sprite currentSprite;
    private Vector3 destLocation;
    public float travelTime;
    public bool dissolve;

    public Transform currentPoint;

    void Awake ()
    {
        rend = GetComponent<SpriteRenderer>();
    }
	public void MoveToPoint(Transform point)
    {
        destLocation = point.position;
        currentPoint = point;
    }
    public void MoveToPoint(Vector3 point)
    {
        destLocation = point;
        currentPoint = null;
    }
    public void MoveToPointThenDestroy(Transform point)
    {
        destLocation = point.position;
        Destroy(gameObject, travelTime + 1f);
    }
    public void ChangeSprite(Sprite newSprite)
    {
        currentSprite = newSprite;
        rend.sprite = currentSprite;
    }

    void Update ()
    {
        if (dissolve)
        {
            FadeOut();
        }
        else
        {
            if (currentPoint != null)
            {
                DebugMove();
            }
            Move();
        }   
	}

    private void FadeOut()
    {
        if (rend.color.a > 0)
        {
            rend.color = Color.Lerp(rend.color, new Color(rend.color.r, rend.color.g, rend.color.b, 0), 0.1f);
        }
        if (rend.color.a < 0.1f)
        {
            Destroy(gameObject);
        }
    }

    private void DebugMove()
    {
        if (destLocation != currentPoint.position)
        {
            MoveToPoint(currentPoint);
        }
    }

    private void Move()
    {
        if (Vector3.Distance(transform.position, destLocation) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, destLocation, 1f / travelTime);
        }
    }
}
