﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEditor;
using UnityEngine;

//OBSOLETE
namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Handles I/O for node editor's csv files
    /// </summary>
    public static class CSV
    {
        public static char[] Delimiter = { ',' };
        
        /// <summary>
        /// Reads line and returns it.
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static string[] ReadLine(StreamReader reader)
        {
            string line = reader.ReadLine();
            if (line == null)
            {
                return null;
            }
            string[] split = line.Split(Delimiter, 3);
            return split;
        }

        public static string[] ReadAllLines(string path)
        {
            return File.ReadAllLines(path);
        }

        /// <summary>
        /// Searches a file for a given GUID and returns the row it appears on.
        /// If there is no result or the file does not exist this returns a -1.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="GUID"></param>
        /// <returns></returns>
        public static int SearchFromFile(string path, string GUID)
        {
            if (!File.Exists(path)) return -1;

            StreamReader reader = new StreamReader(path);
            int row = 0;
            string[] split = ReadLine(reader);
            while (split != null)
            {
                if (split[0].Equals(GUID))
                {
                    reader.Close();
                    return row;
                }

                row++;
                split = ReadLine(reader);
            }
            reader.Close();
            return -1;
        }

        /// <summary>
        /// Reads/skips a specified amount of lines and returns the last line read.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public static string[] GetRowFromFile(string path, int row)
        {
            if (!File.Exists(path)) return null;

            StreamReader reader = new StreamReader(path);
            string[] line = null;
            for (int i = 0; i < row; i++)
            {
                line = ReadLine(reader);
            }

            reader.Close();
            return line;
        }

        /// <summary>
        /// Writes GUID to a specified row, done kind of lazily. Can be optimized using streamreader/writer instead.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="row"></param>
        /// <param name="GUID"></param>
        public static void SetGUIDToFile(string path, int row, string GUID)
        {
            string[] file = File.ReadAllLines(path);
            if (row >= file.Length) return;

            string[] line = file[row].Split(Delimiter, 3);
            string newLine = GUID + "," + line[1] + "," + line[2];
            //Debug.Log(newLine);

            file[row] = newLine;
            Debug.Log(file[row]);
            File.WriteAllLines(path, file);
        }
    }
}
