﻿using UnityEditor;
using UnityEngine;
using System.IO;

namespace Assets.Scripts.Tools.Editor
{
    //Todo: recognize when there's a GUID reassignment or if the GUID does not exist in the node tree (so that the T/F for GUIDs are updated and consistent)
    //Todo: strip quotations
    public class CSVViewer : EditorWindow
    {
        private const float ROW_HEIGHT = 20F;
        private const float ROW_WIDTH_GUID = 70F;
        private const float ROW_WIDTH_SPEAKER = 100F;
        private const float CLEAR_BUTTON_WIDTH = 100F;
        public Node node;
        private Vector2 scrollPos;
        private string[] file;
        public string path;// = "C:\\Users\\Kevintops\\MyProjects\\Unity Projects\\MGD2018\\sprint-3\\Assets\\Resources\\Text\\Test.csv";
        public static char[] Delimiter = { ',' };
        private bool changed = false;


        //[MenuItem("Tools/CSV Viewer")]
        public static void OpenWindow()
        {
            // Get existing open window or if none, make a new one:
            CSVViewer window = GetWindow<CSVViewer>();
            window.titleContent = new GUIContent("CSV Viewer");
            window.Show();
            FocusWindowIfItsOpen<CSVViewer>();
        }

        private void OnDisable()
        {
            node = null;
        }

        private void OnDestroy()
        {
            node = null;
        }

        private void OnEnable()
        {   //can't do file read from here (hot controls or something)
            if (path == null) return;
            changed = false;
        }

        private void OnGUI()
        {
            if (changed || file == null)
            {
                file = File.ReadAllLines(path);
                changed = false;
            }



            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(position.width), GUILayout.Height(position.height));
            EditorGUILayout.BeginHorizontal("Label");

            var lc1 = new GUIContent();
            lc1.text = "<b>Assigned</b>";
            var ls1 = new GUIStyle();
            ls1.alignment = TextAnchor.MiddleLeft;
            ls1.fixedWidth = ROW_WIDTH_GUID;
            GUILayout.Label(lc1, ls1); 

            var lc2 = new GUIContent();
            lc2.text = "<b>Speaker</b>";
            var ls2 = new GUIStyle();
            ls2.alignment = TextAnchor.MiddleLeft;
            ls2.fixedWidth = ROW_WIDTH_SPEAKER;
            GUILayout.Label(lc2, ls2); 

            var lc3 = new GUIContent();
            lc3.text = "<b>Dialogue</b>";
            var ls3 = new GUIStyle();
            ls3.alignment = TextAnchor.MiddleLeft;
            GUILayout.Label(lc3, ls3);

            //Clear GUIDs assigned from CSV file
            if (GUILayout.Button("Clear GUIDs"))
            {
                for (int i = 1; i < file.Length; i++)
                {
                    SetGUIDToFile(path, i, "");
                    changed = true;
                }
            }
            EditorGUILayout.EndHorizontal();
            
            //CSV Viewer buttons
            for (int i = 1; i < file.Length; i++)
            {

                var split = file[i].Split(CSV.Delimiter, 3);

                var b = EditorGUILayout.BeginHorizontal("Button");
                
                if (GUI.Button(b, GUIContent.none))
                {
                    var ret = new string[] { split[1], split[2] };
                    SetGUIDToFile(path, i, node.GUID);
                    node.buffer = ret;
                    changed = true;
                    this.Close();
                }

                //guid bool
                //if guid is not assigned and empty
                var c1 = new GUIContent();
                c1.text = "<b>" + (!split[0].Equals("")).ToString() + "</b>";
                var s1 = new GUIStyle();
                s1.alignment = TextAnchor.MiddleLeft;
                s1.fixedWidth = ROW_WIDTH_GUID;
                GUILayout.Label(c1, s1); //instead of the whole string just show T/F
                
                //speaker
                var c2 = new GUIContent();
                c2.text = split[1];
                var s2 = new GUIStyle();
                s2.alignment = TextAnchor.MiddleLeft;
                s2.fixedWidth = ROW_WIDTH_SPEAKER;
                GUILayout.Label(c2, s2); 

                //dialogue
                var c3 = new GUIContent();
                c3.text = split[2];
                var s3 = new GUIStyle();
                s3.alignment = TextAnchor.MiddleLeft;
                GUILayout.Label(c3, s3); 

                EditorGUILayout.EndHorizontal();

            }

            EditorGUILayout.EndScrollView();
        }

        private void SetGUIDToFile(string path, int row, string GUID)
        {
            string[] file = File.ReadAllLines(path);
            if (row >= file.Length) return;

            string[] line = file[row].Split(Delimiter, 3);
            string newLine = GUID + "," + line[1] + "," + line[2];

            file[row] = newLine;
            File.WriteAllLines(path, file);
        }
    }
}
