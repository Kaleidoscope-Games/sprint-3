﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Handles all view aspects in a MVC model.
    /// </summary>
    public partial class NodeEditor
    {
        private Vector2 drag;
        private Vector2 offset;

        /// <summary>
        /// Draws the the large and smaller grids that overlay on top of eachother
        /// </summary>
        /// <param name="gridSpacing"></param>
        /// <param name="gridOpacity"></param>
        /// <param name="gridColor"></param>
        private void DrawGrid(float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(position.width / gridSpacing);
            int heightDivs = Mathf.CeilToInt(position.height / gridSpacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            offset += drag * 0.5f;
            Vector3 newOffset = new Vector3(offset.x % gridSpacing, offset.y % gridSpacing, 0);

            for (int i = 0; i < widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset, new Vector3(gridSpacing * i, position.height, 0f) + newOffset);
            }

            for (int j = 0; j < heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset, new Vector3(position.width, gridSpacing * j, 0f) + newOffset);
            }

            Handles.EndGUI();
        }

        /// <summary>
        /// Draws connection line when a connection point is clicked but not connected (following the cursor)
        /// </summary>
        /// <param name="e"></param>
        /// <param name="point"></param>
        private void DrawConnectionLine(Event e, ConnectionPoint point)
        {
            var mod = point.type == ConnectionPointType.In ? -1 : 1; 
            Handles.DrawBezier(
                point.rect.center,
                e.mousePosition,
                point.rect.center - mod * Vector2.left * 50f,
                e.mousePosition + mod * Vector2.left * 50f,
                Color.white,
                null,
                2f
            );

            GUI.changed = true;
        }

        /// <summary>
        /// Draws every node in nodes list
        /// </summary>
        private void DrawNodes()
        {
            if (tempAsset == null) return;
            for (int i = 0; i < tempAsset.nodes.Count; i++)
            {
                tempAsset.nodes[i].Draw();
            }
        }

        /// <summary>
        /// Draws every connection in connections list
        /// </summary>
        private void DrawConnections()
        {
            if (tempAsset == null) return;
            for (int i = 0; i < tempAsset.connections.Count; i++)
            {
                tempAsset.connections[i].Draw();
            }
        }
    }
}
