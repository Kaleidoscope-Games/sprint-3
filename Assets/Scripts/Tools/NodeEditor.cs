﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

//Speaker Node == Dialogue Node
//Todo: There's a problem in the clear canvas function (ghost nodes are still in the temp asset)
namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// The controller of a MVC model.
    ///
    /// Note: IDs are generated using their index in the list and is in hex (x8) format.
    /// </summary>
    [System.Serializable]
    public partial class NodeEditor : EditorWindow
    {
        private static ConnectionPoint selectedInPoint;
        private static ConnectionPoint selectedOutPoint;
        public static string defaultDatabase = "";

        private static EditorNodeTree tempAsset;
        public static GameManager Game;
        public static string PreviousNode;
        //public static string CurrentNode;

        [MenuItem("Tools/Conversation Node Editor")]
        private static void OpenWindow()
        {
            NodeEditor window = GetWindow<NodeEditor>();
            window.titleContent = new GUIContent("Conversation Node Editor");
            FocusWindowIfItsOpen<NodeEditor>();
        }

        private void OnEnable()
        {
            var nodes = new List<Node>();
            var connections = new List<Connection>();
            tempAsset = CreateInstance<EditorNodeTree>();
            tempAsset.Init(nodes, connections);
            tempAsset.name = "temp";
            AssetDatabase.CreateAsset(tempAsset, "Assets/Resources/Dump/temp.asset");
            AssetDatabase.Refresh();
        }

        private void OnDestroy()
        {
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(tempAsset));
            AssetDatabase.Refresh();
        }

        private void OnGUI()
        {
            //to track the game as editor window is up: either plug directly into the game's nodetree
            //or run a simulation of it and keep track of where the game is
            //former can't be done unless you rebuild the node connections for the editor (instead of ignoring them
            //as we currently do)
            //if (Application.isPlaying)
            //{
            //    if (tempAsset != null)
            //    {
            //        Game = GameManager.Instance;
            //        LoadFromAsset(Game.ScenePaths[Game.CurrentScene]);
            //        Debug.LogWarning("NodeEditor is now monitoring the game. Don't touch the nodes.");
            //    }

            //}
            DrawGrid(20, 0.2f, Color.gray); //smaller grids
            DrawGrid(100, 0.4f, Color.gray); //larger grids

            DrawNodes();
            DrawConnections();

            CheckConnectionLine(Event.current);

            ProcessNodeEvents(Event.current);
            ProcessEvents(Event.current);
            ProcessMenuButtons();

            if (GUI.changed) Repaint();
        }

        //private void Update()
        //{
        //    if (Application.isPlaying)
        //    {
        //        if (PreviousNode != Game.CurrentNode)
        //        {
        //            PreviousNode = Game.CurrentNode;
        //            Repaint();
        //        }
        //    }
        //}

        #region Process context
        /// <summary>
        /// Process mouse events
        /// </summary>
        /// <param name="e"></param>
        private void ProcessEvents(Event e)
        {
            drag = Vector2.zero;

            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        ClearConnectionSelection();
                    }
                    if (e.button == 1)
                    {
                        ProcessContextMenu(e.mousePosition);
                    }
                    break;
                case EventType.MouseDrag:
                    if (e.button == 2)
                    {
                        OnDrag(e.delta);
                    }
                    break;
            }
        }
        
        /// <summary>
        /// Process events that happen over nodes
        /// </summary>
        /// <param name="e"></param>
        private void ProcessNodeEvents(Event e)
        {
            if (tempAsset.nodes != null)
            {
                for (int i = tempAsset.nodes.Count - 1; i >= 0; i--)
                {
                    bool guiChanged = tempAsset.nodes[i].ProcessEvents(e);

                    if (guiChanged)
                    {
                        GUI.changed = true;
                    }
                }
            }
        }

        /// <summary>
        /// Process context menu when right clicking empty canvas
        /// </summary>
        /// <param name="mousePosition"></param>
        private void ProcessContextMenu(Vector2 mousePosition)
        {
            GenericMenu genericMenu = new GenericMenu();
            genericMenu.AddItem(new GUIContent("Add dialogue node"), false, () => OnClickAddNode<DialogueNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add answer node"), false, () => OnClickAddNode<AnswerNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add condition node"), false, () => OnClickAddNode<ConditionNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add decision node"), false, () => OnClickAddNode<DecisionNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add reward node"), false, () => OnClickAddNode<RewardNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add panel node"), false, () => OnClickAddNode<PanelNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add scene node"), false, () => OnClickAddNode<SceneNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add entry node"), false, () => OnClickAddNode<EntryNode>(mousePosition));
            genericMenu.AddItem(new GUIContent("Add exit node"), false, () => OnClickAddNode<ExitNode>(mousePosition));

            genericMenu.ShowAsContext();
        }

        /// <summary>
        /// Process menu options that sit at the top
        /// </summary>
        private void ProcessMenuButtons()
        {

            //loading
            if (GUI.Button(new Rect(0, 0, 100, 20), "Load Tree"))
            {
                try
                {
                    LoadFromAsset();
                    FocusWindowIfItsOpen<NodeEditor>();

                }
                catch (System.ArgumentOutOfRangeException) { }
            }
            //saving
            if (GUI.Button(new Rect(100, 0, 100, 20), "Save Tree"))
            {
                try
                {
                    SaveToAsset();
                    FocusWindowIfItsOpen<NodeEditor>();

                }
                catch
                {

                }
            }
            //clear
            if (GUI.Button(new Rect(200, 0, 100, 20), "Clear Canvas"))
            {   //Todo: needs to actually delete assets as well
                tempAsset.nodes.Clear();
                tempAsset.connections.Clear();
            }
            //testing
            if (GUI.Button(new Rect(300, 0, 100, 20), "Print Nodes"))
            {
                foreach (var node in tempAsset.nodes)
                {
                    Debug.Log(node);
                }
            }
            //default database
            if (GUI.Button(new Rect(400, 0, 120, 20), "Default Database"))
            {
                defaultDatabase = EditorUtility.OpenFilePanel("Open tree asset", Application.dataPath + "/Resources/Text", "csv");
                defaultDatabase = defaultDatabase.Remove(0, Application.dataPath.Length - "Assets".Length);
            }
            defaultDatabase = GUI.TextField(new Rect(520, 0, 400, 20), defaultDatabase);
        }
        #endregion
        #region On events
        /// <summary>
        /// For dragging (panning) the main editor window
        /// </summary>
        /// <param name="delta"></param>
        private void OnDrag(Vector2 delta)
        {
            drag = delta;

            if (tempAsset.nodes != null)
            {
                for (int i = 0; i < tempAsset.nodes.Count; i++)
                {
                    //drags all the nodes (connections continually draw based on nodes, so by extension those too)
                    tempAsset.nodes[i].Drag(delta); 
                }
            }

            GUI.changed = true;
        }

        /// <summary>
        /// Behavior when nodes are removed from right clicking node objects
        /// </summary>
        /// <param name="node"></param>
        public static void OnClickRemoveNode(Node node)
        {
            if (tempAsset.connections != null)
            {
                List<Connection> connectionsToRemove = new List<Connection>();

                for (int i = 0; i < tempAsset.connections.Count; i++)
                {   //create a list to mark for deletion before actually deleting it
                    if (tempAsset.connections[i].InPointGUID == node.inPoint.NodeID ||
                        tempAsset.connections[i].OutPointGUID == node.outPoint.NodeID)
                    {
                        if (tempAsset.connections[i].InPointGUID == node.inPoint.NodeID)
                        {   //if there is an inpoint (a potential "next node"), delete the next node record from the connecting outpoint
                            tempAsset.connections[i].OutPoint.node.serializableNextNodes.Remove(tempAsset.connections[i].InPointGUID);
                        }
                        connectionsToRemove.Add(tempAsset.connections[i]);
                    }
                }

                for (int i = 0; i < connectionsToRemove.Count; i++)
                {
                    tempAsset.connections.Remove(connectionsToRemove[i]);
                }
            }

            tempAsset.nodes.Remove(node);
            if (tempAsset != null)
            {
                DestroyImmediate(node, true);
                AssetDatabase.Refresh();
            }
            RebuildIDs(tempAsset.nodes);
        }

        /// <summary>
        /// Generic method for adding nodes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="mousePosition"></param>
        private void OnClickAddNode<T>(Vector2 mousePosition) where T : Node
        {
            var nd = CreateInstance<T>();
            nd.Init(mousePosition, System.Guid.NewGuid().ToString(), tempAsset.nodes.Count.ToString("x8"));
            tempAsset.nodes.Add(nd);
            if (tempAsset != null)
                AssetDatabase.AddObjectToAsset(nd, tempAsset);
        }

        /// <summary>
        /// Behavior when input points are clicked (though the draw function is still called)
        /// </summary>
        /// <param name="inPoint"></param>
        public static void OnClickInPoint(ConnectionPoint inPoint)
        {
            selectedInPoint = inPoint;

            if (selectedOutPoint == null) return;
            if (selectedOutPoint.node != selectedInPoint.node)
            {
                CreateConnection(); 
            }

            ClearConnectionSelection();
        }
        /// <summary>
        /// Behavior when output points are clicked (though the draw function is still called)
        /// </summary>
        /// <param name="outPoint"></param>
        public static void OnClickOutPoint(ConnectionPoint outPoint)
        {
            selectedOutPoint = outPoint;

            if (selectedInPoint == null) return; // if clicked on empty space
            if (selectedOutPoint.node != selectedInPoint.node) // if selected points not itself
            {
                //creates a connection that follows the mouse
                CreateConnection();
            }

            ClearConnectionSelection();
        }
        
        /// <summary>
        /// Behavior when rectangle caps are clicked
        /// </summary>
        /// <param name="connection"></param>
        public static void OnClickRemoveConnection(Connection connection)
        {
            var nd = connection.OutPoint.node;

            nd.serializableNextNodes.Remove(connection.InPointGUID); //remove "next node" record from connected node
            tempAsset.connections.Remove(connection);
        }

        #endregion
        #region Helper functions


        /// <summary>
        /// Check whether connection line is still valid
        /// </summary>
        /// <param name="e"></param>
        private void CheckConnectionLine(Event e)
        {
            if (selectedInPoint != null && selectedOutPoint == null)
            {
                DrawConnectionLine(e, selectedInPoint);
            }

            if (selectedOutPoint != null && selectedInPoint == null)
            {
                DrawConnectionLine(e, selectedOutPoint);
            }
        }

        /// <summary>
        /// Rebuild ID numbers so that there aren't weird gaps
        /// </summary>
        /// <param name="list"></param>
        private static void RebuildIDs(List<Node> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].ID = i.ToString("x8");
            }
        }

        /// <summary>
        /// Create and add connections to internal list
        /// </summary>
        private static void CreateConnection()
        {
            tempAsset.connections.Add(new Connection(selectedInPoint, selectedOutPoint));
            selectedOutPoint.node.serializableNextNodes.Add(selectedInPoint.node.GUID);//add connection to serializable nodes
        }

        /// <summary>
        /// Clear internal memory of clicked point
        /// </summary>
        private static void ClearConnectionSelection()
        {
            selectedInPoint = null;
            selectedOutPoint = null;
        }

        //Todo: fix bug where you can only save once before it basically doesn't work anymore (temp file indicates it is fine)
        private void SaveToAsset()
        {
            var path = EditorUtility.SaveFilePanel("Save tree asset", Application.dataPath + "/Resources", "New Tree", "asset");
            path = path.Remove(0, Application.dataPath.Length - "Assets".Length);

            if (File.Exists(path))
            {
                AssetDatabase.DeleteAsset(path);
                AssetDatabase.Refresh();
            }

            AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(tempAsset), path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        //Todo: fix bug where you load and can't connect to newly created nodes
        //      Reverse scenario of when nd.inPoint.node and etc were not reassigned to proper nodes,
        //      the nodes would not connect to themselves and only newly created nodes
        //      Could probably fix by forcing assets to be refreshed? Make them somehow appear in temp file before actually saving? idk
        private void LoadFromAsset(string path = "")
        {
            if (path == "")
            {
                path = EditorUtility.OpenFilePanel("Open tree asset", Application.dataPath + "/Resources", "asset");
                path = path.Remove(0, Application.dataPath.Length - "Assets".Length);
            }
            if (path.Length != 0)
            {
                var tempPath = AssetDatabase.GetAssetPath(tempAsset);

                AssetDatabase.CopyAsset(path, tempPath);
                AssetDatabase.Refresh();
                tempAsset = AssetDatabase.LoadAssetAtPath<EditorNodeTree>(tempPath);

                //find a way to do this in deserialization
                foreach (var connection in tempAsset.connections)
                {
                    foreach (var nd in tempAsset.nodes)
                    {
                        nd.inPoint.node = nd; //fixes reference to its own node (nd.insPoint.node is null)
                        nd.outPoint.node = nd;
                        if (nd.GUID == connection.InPointGUID)
                        {
                            connection.Reconnect(nd.inPoint);
                        }
                        if (nd.GUID == connection.OutPointGUID)
                        {
                            connection.Reconnect(nd.outPoint);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
