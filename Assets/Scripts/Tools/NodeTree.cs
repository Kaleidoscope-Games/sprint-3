﻿using UnityEngine;
using System.Collections.Generic;
namespace Assets.Scripts.Tools.Editor
{
    /// <summary>
    /// Scriptable object used to encapsulate necessary information to draw and hold information about the node tree
    /// </summary>
    [SerializeField]
    public class NodeTree : ScriptableObject, ISerializationCallbackReceiver
    {
        public List<Node> nodes;
        public List<Connection> connections;

        /// <summary>
        /// Initialization method that is basically a constructor method because Unity sucks
        /// </summary>
        public void Init(List<Node> nodes, List<Connection> connections)
        {
            this.nodes = nodes;
            this.connections = connections;
        }

        public void OnBeforeSerialize() { }

        public void OnAfterDeserialize()
        {
            try
            {
                foreach (var connection in connections)
                {
                    //done so that serialization isn't cyclic (infinite) and to rebuild references
                    foreach (var node in nodes)
                    {   
                        //reconnect connection points to nodes
                        node.inPoint.Reconnect(node);
                        node.outPoint.Reconnect(node);
                        //reconnect connection points to nodes for drawing connections
                        if (node.GUID == connection.InPointGUID) connection.Reconnect(node.inPoint);
                        if (node.GUID == connection.OutPointGUID) connection.Reconnect(node.outPoint);
                    }
                }
            }
            catch (System.NullReferenceException) { }
        }
    }
}
