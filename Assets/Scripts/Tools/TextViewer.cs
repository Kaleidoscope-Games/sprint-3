﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Tools.Editor
{
    public class TextViewer : EditorWindow {
        private float toolbarHeight = 20F;
        private static string text = "";

        public static void OpenWindow(string str = "")
        {
            // Get existing open window or if none, make a new one:
            TextViewer window = GetWindow<TextViewer>();
            window.titleContent = new GUIContent("Text Viewer");
            window.Show();
            FocusWindowIfItsOpen<TextViewer>();
            text = str;
        }

        private void OnGUI()
        {
            GUIStyle newStyle = new GUIStyle(EditorStyles.miniButton); //it breaks if I use GUIStyles (null reference)
            newStyle.normal.background = newStyle.active.background;
            if (GUI.Button(new Rect(0, 0, position.width, toolbarHeight), "Text Wrap",
                EditorStyles.textField.wordWrap ? newStyle : EditorStyles.miniButton))
                EditorStyles.textField.wordWrap = !EditorStyles.textField.wordWrap;
            EditorGUI.TextField(new Rect(0, toolbarHeight, position.width, position.height - toolbarHeight), text);
        }
    }
}
